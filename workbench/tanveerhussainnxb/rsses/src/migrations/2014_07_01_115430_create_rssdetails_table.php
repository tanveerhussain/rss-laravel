<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRssdetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rssdetails', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('rss_id');
			$table->foreign('rss_id')->references('id')->on('rss')->onDelete('cascade')->onUpdate('cascade');
			$table->string('title', 255);
			$table->string('link', 255);
			$table->string('extras', 255);
			$table->text('description', 255);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rssdetails');
	}

}
