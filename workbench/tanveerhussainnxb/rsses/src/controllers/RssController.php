<?php

class RssController extends Controller {

	
	/**
	 * Display a listing of the rss if user is logged in.
	 *
	 * @return Response
	 */
	public function index()
	{ 
		/*
		  Check user is logged in then display rss listing page
		  If user is not logged in then redirect to user login page
		*/
		
		if (Auth::check()) { 
		// get all the rss
		$rsses = Rss::all(); 
		
		// load the view and pass the rss data
		return View::make('rsses::index')
					->with('rsses', $rsses);
		}else { 
			return Redirect::to('user/login');
		}

	}


	/**
	 * Show the form for creating a new rss.
	 *
	 * @return Response
	 */
	public function create()
	{ 
		/*
		  Check user is logged in then display create rss page
		  If user is not logged in then redirect to user login page
		*/
			
		if (Auth::check()) { 
		 // load the create form 
		 return View::make('rsses::create');
		}else { 
		  return Redirect::to('user/login');
		}
	}


	/**
	 * Store a newly created rss in database.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate the info, create rules for the inputs
		$rules = array(
			'url'       => 'required|url|unique:rsses'
		);
		
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('rsses/create')
				->withErrors($validator) 
				->withInput(Input::except('password'));  
		} else {
			// create our rss data for the storage
			$rss = new Rss;
			$rss->url = Input::get('url');
			$rss->save();
			
			$insertedId = $rss->id;
			
			$content = file_get_contents(Input::get('url'));
			$rss = new SimpleXmlElement($content);
			
			if(isset($rss->channel)){
				foreach ($rss->channel->item as $item) { 
				$title = '';
				$link = '';
				$description = '';				
				$extras = '';				
				foreach($item as $index=>$row){
					if($index == 'title'){
						$title = $row;	
					}
					if($index == 'link'){
						$link = $row;	
					}					
					if($index == 'description'){
						$description = $row;	
					}
				}
				
				$rss_detail = new Rssdetail;
				$rss_detail->rss_id = $insertedId;
				$rss_detail->title = $title;
				$rss_detail->link = $link;
				$rss_detail->extras = $extras;
				$rss_detail->description = strip_tags($description);
				$rss_detail->save();
			}
			}else if(isset($rss->entry)){
				foreach ($rss->entry as $item) { 
				
				$title = '';
				$link = '';
				$description = '';				
				$extras = '';				
				foreach($item as $index=>$row){
					if($index == 'title'){
						$title = $row;	
					}
					if($index == 'link'){
						$link = $row;	
					}					
					if($index == 'summary'){
						$description = $row;	
					}
				}
				
				$rss_detail = new Rssdetail;
				$rss_detail->rss_id = $insertedId;
				$rss_detail->title = $title;
				$rss_detail->link = $link;
				$rss_detail->extras = $extras;
				$rss_detail->description = strip_tags($description);
				$rss_detail->save();
			}
			}

			// redirect to rss listing page with success message
			Session::flash('message', 'Successfully created Rss Feed!');
			return Redirect::to('rsses');
		}

	}
	

	/**
	 * Display the specified rss.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		/*
		  Check user is logged in then display rss detail page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
		
		// get the rss from database using id
		$rsses = Rss::find($id)->rssdetail;
		
		// show the view and pass the rss to it
		return View::make('rsses::show')
			->with('rsses', $rsses);
			
		}else { 
			return Redirect::to('user/login');
		}

	}


	/**
	 * Show the form for editing the specified rss.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		/*
		  Check user is logged in then display rss edit page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) {
		
		// get the rss from database using id
		$rss = Rss::find($id);

		// show the edit form and pass the rss
		return View::make('rsses::edit')
			->with('rss', $rss);
			
		}else {
			return Redirect::to('user/login');
		}

	}


	/**
	 * Update the specified rss in database.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// validate the info, create rules for the inputs
		$rules = array(
			'url'       => 'required|url|unique:rsses,url,'.$id
		);
		
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('rsses/' . $id . '/edit')
				->withErrors($validator) 
				->withInput(Input::except('password')); 
		} else {
			// Update rss into database using id
			$rss = Rss::find($id);
			$rss->url = Input::get('url');
			$rss->save();
			
			
			
			$insertedId = $id;
			Rssdetail::where('rss_id', '=', $insertedId)->delete();
			
			$content = file_get_contents(Input::get('url'));
			$rss = new SimpleXmlElement($content);
			
			if(isset($rss->channel)){
				foreach ($rss->channel->item as $item) { 
				$title = '';
				$link = '';
				$description = '';				
				$extras = '';				
				foreach($item as $index=>$row){
					if($index == 'title'){
						$title = $row;	
					}
					if($index == 'link'){
						$link = $row;	
					}					
					if($index == 'description'){
						$description = $row;	
					}
				}
				
				$rss_detail = new Rssdetail;
				$rss_detail->rss_id = $insertedId;
				$rss_detail->title = $title;
				$rss_detail->link = $link;
				$rss_detail->extras = $extras;
				$rss_detail->description = strip_tags($description);
				$rss_detail->save();
			}
			}else if(isset($rss->entry)){
				foreach ($rss->entry as $item) { 
				
				$title = '';
				$link = '';
				$description = '';				
				$extras = '';				
				foreach($item as $index=>$row){
					if($index == 'title'){
						$title = $row;	
					}
					if($index == 'link'){
						$link = $row;	
					}					
					if($index == 'summary'){
						$description = $row;	
					}
				}
				
				$rss_detail = new Rssdetail;
				$rss_detail->rss_id = $insertedId;
				$rss_detail->title = $title;
				$rss_detail->link = $link;
				$rss_detail->extras = $extras;
				$rss_detail->description = strip_tags($description);
				$rss_detail->save();
			}
			}
			
			

			// redirect to rss listing page with success message
			Session::flash('message', 'Successfully updated rss!');
			return Redirect::to('rsses');
		}

	}


	/**
	 * Remove the specified rss from database.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete rss from database using id
		$rss = Rss::find($id);
		$rss->delete();
		
		$insertedId = $id;
		Rssdetail::where('rss_id', '=', $insertedId)->delete();

		// redirect to rss listing page with success message
		Session::flash('message', 'Successfully deleted the rss!');
		return Redirect::to('rsses');

	}

}
