<!--Import HTML layout using extends-->
@extends('rsses::rsses')

<!--Update web page title section-->
@section('title')
    Show Rss Feed
@stop


@section('content')
<h1>Show Rss Feed</h1>

@if($rsses)
<table class="table table-striped table-bordered">
	@foreach($rsses as $key => $value)
	<tr>
    	<td align="left" valign="top"><a href="{{ $value->link }}" target="_blank" >{{ $value->title }}</a></td> 
    </tr>
    @endforeach
</table>
@endif

@stop