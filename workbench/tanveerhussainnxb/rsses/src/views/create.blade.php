<!--Import HTML layout using extends-->
@extends('rsses::rsses')

<!--Update web page title section-->
@section('title')
    Create a Rss Feed
@stop


@section('content')
<h1>Create a Rss Feed</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

<!--Create form using laravel core feature-->	
{{ Form::open(array('url' => 'rsses')) }}

	<!--Create Name field with label-->	
	<div class="form-group">
		{{ Form::label('url', 'URL') }}
		{{ Form::text('url', Input::old('url'), array('class' => 'form-control')) }}
	</div>

	<!--Create submit button-->	
	{{ Form::submit('Create the Rss Feed!', array('class' => 'btn btn-primary')) }}

<!--End form-->
{{ Form::close() }}

@stop