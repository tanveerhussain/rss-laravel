<!--Import HTML layout using extends-->
@extends('rsses::rsses')

<!--Update web page title section-->
@section('title')
    Update a Rss Feed
@stop


@section('content')
<h1>Update a Rss Feed</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

<!--Create form using laravel core feature-->	
{{ Form::model($rss, array('route' => array('rsses.update', $rss->id), 'method' => 'PUT')) }}

	<!--Create Name field with label-->	
	<div class="form-group">
		{{ Form::label('url', 'URL') }}
		{{ Form::text('url', null, array('class' => 'form-control')) }}
	</div>

	<!--Create submit button-->	
	{{ Form::submit('Edit the Rss Feed!', array('class' => 'btn btn-primary')) }}

<!--End form-->
{{ Form::close() }}

@stop