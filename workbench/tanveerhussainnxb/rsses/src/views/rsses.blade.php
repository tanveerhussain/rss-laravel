<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--Web page title-->
<title>@yield('title')</title>
<link rel="stylesheet" href="{{asset('packages/tanveerhussainnxb/rsses/css/rss.css')}}" media="screen" type="text/css" />
</head>

<body>
<div class="container"> 
  
  <!--Navigation links for customer-->
  <nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
      <li><a href="{{ URL::to('rsses') }}">View All Rss Feeds</a></li>
      <li><a href="{{ URL::to('rsses/create') }}">Create a Rss</a>
    </ul>
    <ul class="nav navbar-nav" style="float:right">
      <li><a href="{{URL::to('user/logout')}}">Logout</a></li>
    </ul>
  </nav>
  
  <!--Web page content--> 
  @yield('content') </div>
</body>
</html>