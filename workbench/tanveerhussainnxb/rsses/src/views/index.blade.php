<!--Import HTML layout using extends-->
@extends('rsses::rsses')

<!--Update web page title section-->
@section('title')
    All Rss Feeds
@stop


@section('content')
<h1>All Rss Feeds</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>ID</td>
			<td>URL</td>
			<td>Actions</td>
		</tr>
	</thead>
	<tbody>    
    @if($rsses)
    @foreach($rsses as $key => $value)
		<tr>
			<td>{{ $value->id }}</td>
			<td>{{ $value->url }}</td>

			<!-- we will also add show, edit, and delete buttons -->
			<td>

				<!-- delete the rss (uses the destroy method DESTROY /rsses/{id} -->
				<!-- we will add this later since its a little more complicated than the other two buttons -->
				{{ Form::open(array('url' => 'rsses/' . $value->id, 'class' => 'pull-right')) }}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Delete this Rss Feed', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}

				<!-- show the rss (uses the show method found at GET /resses/{id} -->
				<a class="btn btn-small btn-success" href="{{ URL::to('rsses/' . $value->id) }}">Show this Rss Feed</a>

				<!-- edit this rss (uses the edit method found at GET /rsses/{id}/edit -->
				<a class="btn btn-small btn-info" href="{{ URL::to('rsses/' . $value->id . '/edit') }}">Edit this Rss Feed</a>

			</td>
		</tr>
	@endforeach
	@endif
    
	</tbody>
</table>

@stop